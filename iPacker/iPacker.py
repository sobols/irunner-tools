# -*- coding: utf-8 -*-

from lxml import etree
import collections
import logging
import os
import zipfile

ProblemInfo = collections.namedtuple('ProblemInfo', 'short_name hr_name test_count time_limit memory_limit input_file output_file')

CHECKER_NAME = 'check.cpp'
#STATEMENT_LANGUAGE = 'russian'
STATEMENT_LANGUAGE = 'english'

IMPORT_XML_TEMPLATE = '''<?xml version="1.0" encoding="utf-8"?>
<taskset>
    <task name="{short_name}" shortName="{short_name}" memoryLimit="{memory_limit}M">
        <metafile name="{input_file}" type="input" alias="in"/>
        <metafile name="{output_file}" type="output" alias="out"/>
        <fileset>
            <file href="%s" type="Checker" lang="GNUCPP472"/>
{extra_files}
        </fileset>
        <testset points="1" timelimit="{time_limit}" count="{test_count}">
            <testFile target="in" href="tests/##"/>
            <testFile target="out" href="tests/##.a"/>
        </testset>
    </task>
</taskset>
''' % (CHECKER_NAME, )

FILE_TEMPLATE = '            <file href="{local_path}" type="{file_type}"/>'


def read_problem_xml(problem_dir):
    problem_root_element = etree.parse(os.path.join(problem_dir, 'problem.xml'))
    short_name = problem_root_element.getroot().get('short-name')
    hr_name = None
    for name in problem_root_element.find('names'):
        if name.get('language') == STATEMENT_LANGUAGE:
            hr_name = name.get('value').strip()

    def get_param(xpath):
        element = problem_root_element.find(xpath)
        return int(element.text.strip())

    tl = get_param('judging/testset/time-limit') // 1000
    ml = get_param('judging/testset/memory-limit') // (2 ** 20)
    tc = get_param('judging/testset/test-count')

    judging = problem_root_element.find('judging')
    inpf = judging.get('input-file')
    outpf = judging.get('output-file')
    return ProblemInfo(short_name, hr_name, tc, tl, ml, inpf, outpf)


def pack_problem(letter, problem_dir):
    info = read_problem_xml(problem_dir)
    short_name = info.short_name
    logging.info('%s: %s', letter, short_name)

    if letter is not None:
        archive_name = 'irunner_{0}-{1}.zip'.format(letter, short_name)
    else:
        archive_name = 'irunner_{0}.zip'.format(short_name)

    logging.info('time limit: %d s, memory limit: %d MB, tests: %s', info.time_limit, info.memory_limit, info.test_count)

    with zipfile.ZipFile(archive_name, mode='w', compression=zipfile.ZIP_DEFLATED) as zf:
        # tests
        tests_dir = os.path.join(problem_dir, 'tests')
        for test in os.listdir(tests_dir):
            zf.write(os.path.join(tests_dir, test), os.path.join('tests', test))

        # checker
        zf.write(os.path.join(problem_dir, CHECKER_NAME), CHECKER_NAME)

        # statement
        statement_dir = os.path.join(problem_dir, 'statements', '.html', STATEMENT_LANGUAGE)
        extra_files = []
        if os.path.isdir(statement_dir):
            for item in os.listdir(statement_dir):
                path_in_zip = os.path.join('statement', item).replace(os.sep, '/')
                zf.write(os.path.join(statement_dir, item), path_in_zip)

                if item == 'problem.html':
                    file_type = 'ConditionMain'
                else:
                    file_type = 'ConditionFile'
                extra_files.append((path_in_zip, file_type))
        else:
            logging.warning('HTML statement for problem %s is not available', short_name)

        # import.xml
        subst = dict(info.__dict__)
        extra_list = []
        for local_path, file_type in extra_files:
            extra_list.append(FILE_TEMPLATE.format(local_path=local_path, file_type=file_type))
        subst['extra_files'] = '\n'.join(extra_list)
        import_xml = IMPORT_XML_TEMPLATE.format(**subst)
        zf.writestr('import.xml', import_xml)


def pack_contest(contest_dir):
    root_element = etree.parse(os.path.join(contest_dir, 'contest.xml'))
    problems_element = root_element.find('problems')
    for problem_element in problems_element:
        letter = problem_element.get('index').upper()
        short_name = problem_element.get('url').split('/')[-1]
        pack_problem(letter, os.path.join(contest_dir, 'problems', short_name))


def main():
    logging.basicConfig(level=logging.DEBUG, format='%(levelname)s - %(message)s')

    try:
        if os.path.isfile('contest.xml'):
            pack_contest('.')
        elif os.path.isfile('problem.xml'):
            pack_problem(None, '.')
        else:
            logging.error('The script must be run inside contest or problem directory')
    except:
        logging.exception('Something went wrong')

    raw_input("Press Enter to finish...")

if __name__ == '__main__':
    main()
