import sys

BEGIN = '''
<table id="langSettings" width="100%" cellpadding="1" cellspacing="0" class="navigator" border="0" style="display: none">
    <tr class="header" bgColor="#E0E0E0" >
        <td class="nav_header" onmouseover="this.bgColor='#FFFFFF';" onmouseout="this.bgColor='';">${loc[20016]}</td>
        <td class="nav_header" onmouseover="this.bgColor='#FFFFFF';" onmouseout="this.bgColor='';">${loc[21350]}</td>
        <td class="nav_header" onmouseover="this.bgColor='#FFFFFF';" onmouseout="this.bgColor='';">${loc[21351]}</td>
    </tr>'''

END = '''</table>'''
ROW = '''    <tr onMouseOver="this.bgColor='#F8F8F8'" onmouseout="this.bgColor='#FFFFFF'">
        <td>{0}</td>
        <td style="font-family: monospace">{1}</td>
        <td style="border-right: 1px solid #B4B4B4; font-family: monospace">{2}</td>
    </tr>'''


def main():
    langs = []

    while True:
        name = sys.stdin.readline().strip()
        if name == 'EOF':
            break

        compileCmd = sys.stdin.readline().strip()
        runCmd = sys.stdin.readline().strip()
        sys.stdin.readline()

        if name is None:
            break
        langs.append((name, compileCmd, runCmd))

    print BEGIN
    for name, compileCmd, runCmd in langs:
        print ROW.format(name, compileCmd, runCmd)
    print END

if __name__ == '__main__':
    main()
